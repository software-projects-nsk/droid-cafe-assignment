package com.example.droidcafe;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;

public class InvoiceActivity extends AppCompatActivity {

    private static final String LOG_TAG = InvoiceActivity.class.getSimpleName();

    private int coffeeQuant, iceCreamQuant, teaQuant, softDrinkQuant, donutQuant;
    private double coffeePrice, iceCreamPrice, teaPrice, softDrinkPrice, donutPrice;
    private double subTotal, grandTotal;
    private final double TAX = 0.06;

    private TextView coffeeQuantText, iceCreamQuantText, teaQuantText, softDrinkQuantText, donutQuantText;
    private TextView coffeePriceText, iceCreamPriceText, teaPriceText, softDrinkPriceText, donutPriceText;
    private TextView subTotalText, taxText, grantTotalText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        Intent intent = getIntent();

        coffeeQuantText = findViewById(R.id.iCoffeeQuant);
        iceCreamQuantText = findViewById(R.id.iIceCreamQuant);
        teaQuantText = findViewById(R.id.iTeaQuant);
        softDrinkQuantText = findViewById(R.id.iSoftDrinkQuant);
        donutQuantText = findViewById(R.id.iDonutQuant);

        coffeePriceText = findViewById(R.id.iCoffeeTotal);
        iceCreamPriceText = findViewById(R.id.iIceCreamTotal);
        teaPriceText = findViewById(R.id.iTeaTotal);
        softDrinkPriceText = findViewById(R.id.iSoftDrinkTotal);
        donutPriceText = findViewById(R.id.iDonutTotal);

        subTotalText = findViewById(R.id.iSubtotal);
        taxText = findViewById(R.id.iTax);
        grantTotalText = findViewById(R.id.iGrandTotal);

        coffeeQuant = intent.getIntExtra("coffeeQ", 0);
        iceCreamQuant = intent.getIntExtra("creamQ", 0);
        teaQuant = intent.getIntExtra("teaQ", 0);
        softDrinkQuant = intent.getIntExtra("drinkQ", 0);
        donutQuant = intent.getIntExtra("donutQ", 0);

        coffeePrice = intent.getDoubleExtra("coffeeP", 0);
        iceCreamPrice = intent.getDoubleExtra("creamP", 0);
        teaPrice = intent.getDoubleExtra("teaP", 0);
        softDrinkPrice = intent.getDoubleExtra("drinkP", 0);
        donutPrice = intent.getDoubleExtra("donutP", 0);

        DisplayInvoice();
    }

    private void DisplayInvoice() {

        NumberFormat format = NumberFormat.getCurrencyInstance();

        coffeeQuantText.setText(coffeeQuant + "x");
        iceCreamQuantText.setText(iceCreamQuant + "x");
        teaQuantText.setText(teaQuant + "x");
        softDrinkQuantText.setText(softDrinkQuant + "x");
        donutQuantText.setText(donutQuant + "x");

        CalculateTotals();

        coffeePriceText.setText(format.format(coffeePrice));
        iceCreamPriceText.setText(format.format(iceCreamPrice));
        teaPriceText.setText(format.format(teaPrice));
        softDrinkPriceText.setText(format.format(softDrinkPrice));
        donutPriceText.setText(format.format(donutPrice));

        CalculateGrandTotals();

        subTotalText.setText(format.format(subTotal));
        taxText .setText(format.format(TAX * subTotal));
        grantTotalText.setText(format.format(grandTotal));
    }

    private void CalculateTotals() {
        coffeePrice *= coffeeQuant;
        iceCreamPrice *= iceCreamQuant;
        teaPrice *= teaQuant;
        softDrinkPrice *= softDrinkQuant;
        donutPrice *= donutQuant;
        subTotal = coffeePrice + iceCreamPrice + teaPrice + softDrinkPrice + donutPrice;
    }

    private void CalculateGrandTotals() {
        grandTotal = (subTotal * TAX) + subTotal;
    }

    public void ReturnToMain(View view) {
        NavUtils.navigateUpFromSameTask(this);
    }
}
