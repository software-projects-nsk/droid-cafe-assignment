package com.example.droidcafe;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ItemViewHolder>  {

    private final List<Drawable> mImageList;
    private final List<String> mItemList;
    private final List<Double> mPriceList;
    //private final List<Integer> mQuantityList;
    private final List<String> mQuantityEditList;
    private LayoutInflater mInflater;

    class ItemViewHolder extends RecyclerView.ViewHolder {
        public final ImageView itemImageView;
        public final TextView itemDescriptionView;
        public final TextView itemPriceView;
        public final EditText itemQuantityView;
        public final MyCustomEditTextListener textListener;
        final ItemListAdapter mAdapter;

        public ItemViewHolder(View itemView, ItemListAdapter adapter, MyCustomEditTextListener listener) {
            super(itemView);
            itemImageView = itemView.findViewById(R.id.itemImage);
            itemDescriptionView = itemView.findViewById(R.id.itemDesc);
            itemPriceView = itemView.findViewById((R.id.itemPrice));
            itemQuantityView = itemView.findViewById(R.id.itemQuantity);
            textListener = listener;
            itemQuantityView.addTextChangedListener(listener);
            this.mAdapter = adapter;
        }
    }

    public ItemListAdapter(Context context, List<Drawable> imageList, List<String> itemList, List<Double> priceList, List<String> quantityEditList) {
        mInflater = LayoutInflater.from(context);
        this.mImageList = imageList;
        this.mItemList = itemList;
        this.mPriceList = priceList;
        //this.mQuantityList = quantityList;
        this.mQuantityEditList = quantityEditList;
    }

    @NonNull
    @Override
    public ItemListAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.shoplist_item, parent, false);
        return new ItemViewHolder(mItemView, this, new MyCustomEditTextListener());
    }

    @Override
    public void onBindViewHolder(@NonNull ItemListAdapter.ItemViewHolder holder, int position) {
        Drawable mImg = mImageList.get(position);
        String mDesc = mItemList.get(position);
        double mPrice = mPriceList.get(position);

        NumberFormat format = NumberFormat.getCurrencyInstance();

        holder.itemImageView.setImageDrawable(mImg);
        holder.itemDescriptionView.setText(mDesc);
        holder.itemPriceView.setText(format.format(mPrice));
        holder.textListener.updatePosition(holder.getAdapterPosition());
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().equals(""))
                mQuantityEditList.set(position, "0");
            else
                mQuantityEditList.set(position, s.toString());
        }

        @Override
        public void afterTextChanged(Editable s) {}
    }
}
