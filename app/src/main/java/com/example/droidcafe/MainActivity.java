package com.example.droidcafe;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private ItemListAdapter mAdapter;

    private int coffeeQuant;
    private int iceCreamQuant;
    private int teaQuant;
    private int softDrinkQuant;
    private int donutQuant;

    Resources res;

    private final ArrayList<Drawable> mImageList = new ArrayList();
    private final List<String> mDescList = Arrays.asList("Coffee", "Ice Cream", "Tea", "Soft Drink", "Donut");
    private final List<Double> mPriceList = Arrays.asList(1.50, 2.50, 1.25, 1.00, 0.55);
    //private final List<Integer> mQuantityList = Arrays.asList(0, 0, 0, 0, 0);
    private final List<String> mTempQuants = Arrays.asList("0", "0", "0", "0", "0");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        res = getResources();

        mImageList.add(res.getDrawable(R.drawable.coffee));
        mImageList.add(res.getDrawable(R.drawable.icecream));
        mImageList.add(res.getDrawable(R.drawable.tea));
        mImageList.add(res.getDrawable(R.drawable.softdrink));
        mImageList.add(res.getDrawable(R.drawable.donut));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchSecondActivity(view);
            }
        });

        mRecyclerView = findViewById(R.id.recyclerview);
        mAdapter = new ItemListAdapter(this, mImageList, mDescList, mPriceList, mTempQuants);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void launchSecondActivity(View view) {
        Intent intent = new Intent(this, InvoiceActivity.class);

        coffeeQuant = Integer.parseInt(mTempQuants.get(0));
        iceCreamQuant = Integer.parseInt(mTempQuants.get(1));
        teaQuant = Integer.parseInt(mTempQuants.get(2));
        softDrinkQuant = Integer.parseInt(mTempQuants.get(3));
        donutQuant = Integer.parseInt(mTempQuants.get(4));

        intent.putExtra("coffeeQ", coffeeQuant);
        intent.putExtra("creamQ", iceCreamQuant);
        intent.putExtra("teaQ", teaQuant);
        intent.putExtra("drinkQ", softDrinkQuant);
        intent.putExtra("donutQ", donutQuant);

        intent.putExtra("coffeeP", mPriceList.get(0));
        intent.putExtra("creamP", mPriceList.get(1));
        intent.putExtra("teaP", mPriceList.get(2));
        intent.putExtra("drinkP", mPriceList.get(3));
        intent.putExtra("donutP", mPriceList.get(4));

        startActivity(intent);
    }


}
